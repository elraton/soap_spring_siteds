
package com.service.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="coError" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="txNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="coIafa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="txPeticion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "coError",
    "txNombre",
    "coIafa",
    "txPeticion"
})
@XmlRootElement(name = "getConsultaDerivaResponse")
public class GetConsultaDerivaResponse {

    @XmlElement(required = true)
    protected String coError;
    @XmlElement(required = true)
    protected String txNombre;
    @XmlElement(required = true)
    protected String coIafa;
    @XmlElement(required = true)
    protected String txPeticion;

    /**
     * Obtiene el valor de la propiedad coError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoError() {
        return coError;
    }

    /**
     * Define el valor de la propiedad coError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoError(String value) {
        this.coError = value;
    }

    /**
     * Obtiene el valor de la propiedad txNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxNombre() {
        return txNombre;
    }

    /**
     * Define el valor de la propiedad txNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxNombre(String value) {
        this.txNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad coIafa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoIafa() {
        return coIafa;
    }

    /**
     * Define el valor de la propiedad coIafa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoIafa(String value) {
        this.coIafa = value;
    }

    /**
     * Obtiene el valor de la propiedad txPeticion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxPeticion() {
        return txPeticion;
    }

    /**
     * Define el valor de la propiedad txPeticion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxPeticion(String value) {
        this.txPeticion = value;
    }

}
