package com.service.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;


public class SecurityHandlerResolver implements HandlerResolver {

public List<Handler> getHandlerChain(PortInfo portInfo) {
  List<Handler> handlerChain = new ArrayList<Handler>();

  SecurityHandler hh = new SecurityHandler();

  handlerChain.add(hh);

  return handlerChain;
   }
}