
package pe.gob.susalud.acreditacion.wspasarelasusalud;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="coError" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="coAfPaciente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="coIafa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="txFecha" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="imFoto" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "coError",
    "coAfPaciente",
    "coIafa",
    "txFecha",
    "imFoto"
})
@XmlRootElement(name = "getFotoResponse")
public class GetFotoResponse {

    @XmlElement(required = true)
    protected String coError;
    @XmlElement(required = true)
    protected String coAfPaciente;
    @XmlElement(required = true)
    protected String coIafa;
    @XmlElement(required = true)
    protected String txFecha;
    @XmlElement(required = true)
    protected byte[] imFoto;

    /**
     * Obtiene el valor de la propiedad coError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoError() {
        return coError;
    }

    /**
     * Define el valor de la propiedad coError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoError(String value) {
        this.coError = value;
    }

    /**
     * Obtiene el valor de la propiedad coAfPaciente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoAfPaciente() {
        return coAfPaciente;
    }

    /**
     * Define el valor de la propiedad coAfPaciente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoAfPaciente(String value) {
        this.coAfPaciente = value;
    }

    /**
     * Obtiene el valor de la propiedad coIafa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoIafa() {
        return coIafa;
    }

    /**
     * Define el valor de la propiedad coIafa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoIafa(String value) {
        this.coIafa = value;
    }

    /**
     * Obtiene el valor de la propiedad txFecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxFecha() {
        return txFecha;
    }

    /**
     * Define el valor de la propiedad txFecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxFecha(String value) {
        this.txFecha = value;
    }

    /**
     * Obtiene el valor de la propiedad imFoto.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getImFoto() {
        return imFoto;
    }

    /**
     * Define el valor de la propiedad imFoto.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setImFoto(byte[] value) {
        this.imFoto = value;
    }

}
