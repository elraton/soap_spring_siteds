
package pe.gob.susalud.acreditacion.wspasarelasusalud;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="coAfPaciente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="coIafa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "coAfPaciente",
    "coIafa"
})
@XmlRootElement(name = "getFotoRequest")
public class GetFotoRequest {

    @XmlElement(required = true)
    protected String coAfPaciente;
    @XmlElement(required = true)
    protected String coIafa;

    /**
     * Obtiene el valor de la propiedad coAfPaciente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoAfPaciente() {
        return coAfPaciente;
    }

    /**
     * Define el valor de la propiedad coAfPaciente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoAfPaciente(String value) {
        this.coAfPaciente = value;
    }

    /**
     * Obtiene el valor de la propiedad coIafa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoIafa() {
        return coIafa;
    }

    /**
     * Define el valor de la propiedad coIafa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoIafa(String value) {
        this.coIafa = value;
    }

}
