package com.elratonmaton.sicafwss;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.service.ws.GetConsultaAsegNomRequest;
import com.service.ws.GetConsultaAsegNomResponse;
import com.service.ws.SecurityHandlerResolver;
import com.service.ws.WSPasarelaSuSalud;
import com.service.ws.WSPasarelaSuSalud_Service;

@RestController
public class Controller {

	@GetMapping("/")
	List<String> all() {
		WSPasarelaSuSalud_Service ServiceImpl= new WSPasarelaSuSalud_Service();
		SecurityHandlerResolver handlerResolver = new SecurityHandlerResolver();
		ServiceImpl.setHandlerResolver(handlerResolver);
        WSPasarelaSuSalud Service = ServiceImpl.getWSPasarelaSuSaludSOAP();
        
        GetConsultaAsegNomRequest request = new GetConsultaAsegNomRequest();
        request.setCoExcepcion("0000");
        request.setTxNombre("270_CON_ASE");
        request.setCoIafa("20001");
        request.setTxPeticion("ISA*00*          *00*          *ZZ*980001C        *ZZ*20001          *171025*1210*|*00501*000000001*0*P*: ~GS*HS*980001C*20001*20171025*121037*1*X*005010~ST*270*0001~BHT*0022*13~HL*1**20*1~NM1*PR*2******PI*20100054184~PRV*OR**CN~HL*2*1*21*1~NM1*1P*2******FI*20001~HL*3*2*22*0~NM1*IL*1*DELGADO*ME****MI*  ***EDUARDO~REF*DD*1~REF*4A* ~REF*PRT* * *ZZ: ~REF*D7* * *  : :: ~REF*D7* **ZZ: ~REF*8X* ~REF*S2* ~REF*ZZ* ~REF*18* **ZZ: ~REF*PRT* ~DTP*447*D8* ~NM1*P5* * * ******** ~REF*DD* **4A: ~SE*5*0001~GE*1*1~IEA*1*000000001~");
        
        
        GetConsultaAsegNomResponse response = Service.getConsultaAsegNom(request);

        System.out.println(response);
		List<String> messages = Arrays.asList("Hello", "World!", "How", "Are", "You");
		return messages;
	}
}