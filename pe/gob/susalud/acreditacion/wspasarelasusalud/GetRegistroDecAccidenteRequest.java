
package pe.gob.susalud.acreditacion.wspasarelasusalud;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="coExcepcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="txNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="coIafa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="txPeticion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="txDescripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "coExcepcion",
    "txNombre",
    "coIafa",
    "txPeticion",
    "txDescripcion"
})
@XmlRootElement(name = "getRegistroDecAccidenteRequest")
public class GetRegistroDecAccidenteRequest {

    @XmlElement(required = true)
    protected String coExcepcion;
    @XmlElement(required = true)
    protected String txNombre;
    @XmlElement(required = true)
    protected String coIafa;
    @XmlElement(required = true)
    protected String txPeticion;
    @XmlElement(required = true)
    protected String txDescripcion;

    /**
     * Obtiene el valor de la propiedad coExcepcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoExcepcion() {
        return coExcepcion;
    }

    /**
     * Define el valor de la propiedad coExcepcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoExcepcion(String value) {
        this.coExcepcion = value;
    }

    /**
     * Obtiene el valor de la propiedad txNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxNombre() {
        return txNombre;
    }

    /**
     * Define el valor de la propiedad txNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxNombre(String value) {
        this.txNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad coIafa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoIafa() {
        return coIafa;
    }

    /**
     * Define el valor de la propiedad coIafa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoIafa(String value) {
        this.coIafa = value;
    }

    /**
     * Obtiene el valor de la propiedad txPeticion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxPeticion() {
        return txPeticion;
    }

    /**
     * Define el valor de la propiedad txPeticion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxPeticion(String value) {
        this.txPeticion = value;
    }

    /**
     * Obtiene el valor de la propiedad txDescripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTxDescripcion() {
        return txDescripcion;
    }

    /**
     * Define el valor de la propiedad txDescripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTxDescripcion(String value) {
        this.txDescripcion = value;
    }

}
